# Download salinity and O2 CMEMS data from ocean navigator

# Dowload the static layers with the bottom depth layer from CMEMS:
# ftp://my.cmems-du.eu/Core/GLOBAL_REANALYSIS_BIO_001_029/global-reanalysis-bio-001-029-statics
# ftp://my.cmems-du.eu/Core/GLOBAL_REANALYSIS_PHY_001_030/global-reanalysis-phy-001-030-statics


# cd to input directory
cd so/input

for year in {2000..2019}; do
    for month in {05..11}; do
        echo $year $month
        name="mercatorglorys12v1_gl12_mean_"${year}${month}".nc"
        filepath="https://navigator.oceansdata.ca/thredds/fileServer/global-reanalysis-phy-001-030-monthly"/${year}/${name}
        if [[ ! -f "$name" ]]; then
            wget --no-hsts $filepath
        fi
    done
done

# cd to input directory
cd ../..
cd o2/input

for year in {2000..2019}; do
    for month in {05..11}; do
        echo $year $month
        name="mercatorfreebiorys2v4_global_mean_"${year}${month}".nc"
        filepath="https://navigator.oceansdata.ca/thredds/fileServer/global-reanalysis-bio-001-029-monthly"/${year}/${name}
        if [[ ! -f "$name" ]]; then
            wget --no-hsts $filepath
        fi
    done
done
